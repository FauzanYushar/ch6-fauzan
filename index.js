const { urlencoded } = require("express")
const express= require("express")
const app = express()
const {user_game} = require("./models")
const {user_game_biodata} = require("./models")
const {user_game_history} = require("./models")


app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(express.static("/public"))

app.set("viewengine", "ejs")


// render
app.get("/user", (req,res)=>{
    res.render("user.ejs")
})

app.get("/user/biodata", (req,res)=>{
    res.render("biodata.ejs")
})

app.get("/user/history", (req,res)=>{
    res.render("history.ejs")
})


//create data
app.post("/user", (req,res)=>{
    const {username,email,password,name,birthdate,address,game,result} = req.body
    user_game.create({
        username: username,
        email: email,
        password: password
    })
    .then((user)=>{
        user_game_biodata.create({
            name: name,
            birthdate: birthdate,
            address: address,
            user_id: user.id
        })
            user_game_history.create({
                game: game,
                result: result,
                user_id: user.id
            })
            .then(()=>{
                res.json({message: `Created successfully`})
            })
        })
    })

app.post("/user/history", (req,res)=>{
    const {game,result,user_id} = req.body
    user_game_history.create({
        game: game,
        result: result,
        user_id: user_id
    })
    .then((history)=>{
        res.json({message: `history on user_id: ${user_id} is submitted successfully`, data: history})
    })
})


//show all data
app.get("/user/showAll", (req,res)=>{
    user_game.findAll({include: [{model: user_game_biodata, as: "biodata"}, {model: user_game_history, as: "history"}]})
    .then((user)=>{
        res.json({message: "show all user data", data: user})
    })
})

app.get("/user/biodata/showAll", (req,res)=>{
    user_game.findAll({include: [{model: user_game_biodata, as: "biodata"}]})
    .then((user_biodata)=>{
        res.json({message: "show all biodata", data: user_biodata})
    })
})

app.get("/user/history/showAll", (req,res)=>{
    user_game.findAll({include: [{model: user_game_history, as: "history"}]})
    .then((user_history)=>{
        res.json({message: "show all history", data: user_history})
    })
})


// show one data
app.get("/user/showOne/:id", (req,res)=>{
    var {id}= req.params
    user_game.findOne({where: {id:id}, include: [{model: user_game_biodata, as: "biodata"}, {model: user_game_history, as: "history"}]})
    .then((user)=>{
        res.json({message: `show one user data with id:${id}`, data: user})
    })
})

app.get("/user/biodata/showOne/:id", (req,res)=>{
    var {id}= req.params
    user_game.findOne({where: {id:id}, include: [{model: user_game_biodata, as: "biodata"}]})
    .then((user_biodata)=>{
        res.json({message: `show one user biodata with id:${id}`, data: user_biodata})
    })
})

app.get("/user/history/showOne/:id", (req,res)=>{
    var {id}= req.params
    user_game.findOne({where: {id:id}, include: [{model: user_game_history, as: "history"}]})
    .then((user_history)=>{
        res.json({message: `show one user history with id:${id}`, data: user_history})
    })
})


//update
app.put("/user/update/:id", (req,res)=>{
    var {id} = req.params
    var {username,email,password,name,birthdate,address} = req.body
    user_game.findOne({where: {id:id}})
    .then((user)=>{
        user.update({
            username: username,
            email: email,
            password: password
        })
    })
    user_game_biodata.findOne({where: {user_id:id}, include: [{model: user_game, as: "user"}]})
    .then((biodata)=>{
        biodata.update({
            name: name,
            birthdate: birthdate,
            address: address
        })
        .then(()=>{
            res.json({message: `user data with id:${id} is updated`})
        })
    })
})

app.put("/user/history/update/:id", (req,res)=>{
    var {id} = req.params
    var {game,result} = req.body
    user_game.findOne({where: {id:id}})
    .then((history)=>{
        history.update({
            game: game,
            result: result
        })
        .then(()=>{
            res.json({message: `history with id:${id} is updated`})
        })
    })
})


//delete
app.delete("/user/delete/:id", (req,res)=>{
    var{id}= req.params
    user_game_biodata.destroy({where: {user_id:id}, include: [{model: user_game, as: "user"}]})
    user_game_history.destroy({where: {user_id:id}, include: [{model: user_game, as: "user"}]})
    user_game.destroy({where: {id:id}})
    .then(()=>{
        res.json({message: `user data, biodata, and history in user id:${id} is deleted`})  
    })
})

app.delete("/user/history/delete/:id", (req,res)=>{
    var{id}= req.params
    user_game_history.destroy({where: {id:id}})
    .then(()=>{
        res.json({message: `user history with id:${id} is deleted`})  
    })
})


app.listen(8000, console.log("listen to http://localhost:8000"))